package eg.edu.alexu.csd.filestructure.redblacktree;

public class Node<T extends Comparable<T>, V> implements INode<T, V> {
	
	T key;
	V value;
	INode<T, V> left,right,parent;
	boolean color;
	
	public Node(T key, V value) {
		super();
		this.key = key;
		this.value = value;
		left=right=parent = null;
		color = RED;
	}

	@Override
	public void setParent(INode<T, V> parent) {
		this.parent = parent;
	}

	@Override
	public INode<T, V> getParent() {
		return parent;
	}

	@Override
	public void setLeftChild(INode<T, V> leftChild) {
		this.left = leftChild;
	}

	@Override
	public INode<T, V> getLeftChild() {
		return left;
	}

	@Override
	public void setRightChild(INode<T, V> rightChild) {
		this.right = rightChild;
	}

	@Override
	public INode<T, V> getRightChild() {
		return right;
	}

	@Override
	public T getKey() {
		return key;
	}

	@Override
	public void setKey(T key) {
		this.key=key;
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public void setValue(V value) {
		this.value=value;
	}

	@Override
	public boolean getColor() {
		return color;
	}

	@Override
	public void setColor(boolean color) {
		this.color=color;
	}

	@Override
	public boolean isNull() {
		if(this == null) {
			return true;
		}else {
			return false;
		}
	}

}
