package eg.edu.alexu.csd.filestructure.redblacktree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TreeMap<T extends Comparable<T>, V> implements ITreeMap<T, V> {
	
	private RedBlackTree<T, V> tree;
	
	public static void main(String[] args) {
		TreeMap<Integer, String> tree_map = new TreeMap<Integer, String>();
		tree_map.put(1, "Noha");
		tree_map.put(7, "Kamal");
		tree_map.put(3, "Abeer");
		tree_map.put(5, "Rashid");
		tree_map.put(20, "Abdo");
		tree_map.pollFirstEntry();
		tree_map.pollLastEntry();
		
		Set<Map.Entry<Integer, String>> s =tree_map.entrySet();
		for (Entry<Integer, String> entry : s) {
			System.out.println(entry.getKey()+": "+entry.getValue());
		}
		
	}
	
	final class MyEntry<T, V> implements Map.Entry<T, V> {
		
		private final T key;
	    private V value;

	    public MyEntry(T key, V value) {
	        this.key = key;
	        this.value = value;
	    }

		@Override
		public T getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V value) {
			V old = this.value;
	        this.value = value;
	        return old;
		}
		
	}

	
	public TreeMap() {
		tree = new RedBlackTree<T,V>();
	}

	@Override
	public Entry<T, V> ceilingEntry(T key) {
		INode node = tree.searchforNode(key);
		if(node == null) {
			return null;
		}
		node = tree.Minimum(node.getRightChild());
		MyEntry<T, V> e = new MyEntry((T)node.getKey(), (V)node.getValue());
		return e;
	}

	@Override
	public T ceilingKey(T key) {
		INode node = tree.searchforNode(key);
		if(node == null) {
			return null;
		}
		node = tree.Minimum(node.getRightChild());
		return (T) node.getKey();
	}

	@Override
	public void clear() {
		tree.clear();		
	}

	@Override
	public boolean containsKey(T key) {
		return tree.contains(key);
	}

	@Override
	public boolean containsValue(V value) {
		Set<Map.Entry<T, V>> s =entrySet();
		for (Entry<T, V> entry : s) {
			if(entry.getValue() == value) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Set<Entry<T, V>> entrySet() {
        LinkedHashMap<T, V> m =   new LinkedHashMap<T, V>(); 
        getSet(m, tree.root);
		return m.entrySet();
	}
	
	private void getSet(LinkedHashMap<T, V> m, INode x) {
		if(x != null) {
			getSet(m, x.getLeftChild());
			m.put((T)x.getKey(),(V) x.getValue());
			getSet(m, x.getRightChild());
		}
	}

	@Override
	public Entry<T, V> firstEntry() {
		INode x = tree.root;
		if(x == null) {
			return null;
		}
		while(x.getLeftChild() != null) {
			x = x.getLeftChild();
		}
		return new MyEntry(x.getKey(), x.getValue());
	}

	@Override
	public T firstKey() {
		return firstEntry().getKey();
	}

	@Override
	public Entry<T, V> floorEntry(T key) {
		INode node = tree.searchforNode(key);
		if(node == null) {
			return null;
		}
		node = tree.Maximum(node.getLeftChild());
		return new MyEntry(node.getKey(), node .getValue());
	}

	@Override
	public T floorKey(T key) {
		return floorEntry(key).getKey();
	}

	@Override
	public V get(T key) {
		Set<Map.Entry<T, V>> s =entrySet();
		for (Entry<T, V> entry : s) {
			if(entry.getKey() == key) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	public ArrayList<Entry<T, V>> headMap(T toKey) {
		ArrayList<Entry<T, V>> m = new ArrayList<Map.Entry<T,V>>();
		getHead(m, tree.root, toKey);
		return m;
	}
	
	private void getHead(ArrayList<Entry<T, V>> m, INode x ,T toKey) {
		if(x != null) {
			getHead(m, x.getLeftChild() ,toKey);
			if(x.getKey().compareTo(toKey) < 0) {
				m.add(new MyEntry((T)x.getKey(),(V) x.getValue()));
			}
			getHead(m, x.getRightChild() ,toKey);
		}
	}

	@Override
	public ArrayList<Entry<T, V>> headMap(T toKey, boolean inclusive) {
		if(inclusive == false) {
			return headMap(toKey);
		}else {
			ArrayList<Entry<T, V>> m = new ArrayList<Map.Entry<T,V>>();
			getHeadINclusive(m, tree.root, toKey);
			return m;	
		}
	}
	
	private void getHeadINclusive(ArrayList<Entry<T, V>> m, INode x ,T toKey) {
		if(x != null) {
			getHead(m, x.getLeftChild() ,toKey);
			if(x.getKey().compareTo(toKey) <= 0) {
				m.add(new MyEntry((T)x.getKey(),(V) x.getValue()));
			}
			getHead(m, x.getRightChild() ,toKey);
		}
	}

	@Override
	public Set<T> keySet() {
		Set<Map.Entry<T, V>> s =entrySet();
		Set<T> keyset = new HashSet<T>();
		for (Entry<T, V> entry : s) {
			keyset.add(entry.getKey());
		}
		return keyset;
	}

	@Override
	public Entry<T, V> lastEntry() {
		INode x = tree.root;
		if(x == null) {
			return null;
		}
		while(x.getRightChild() != null) {
			x = x.getRightChild();
		}
		return new MyEntry(x.getKey(), x.getValue());
	}

	@Override
	public T lastKey() {
		return lastEntry().getKey();
	}

	@Override
	public Entry<T, V> pollFirstEntry() {
		INode x = tree.root;
		if(x == null) {
			return null;
		}
		while(x.getLeftChild() != null) {
			x = x.getLeftChild();
		}
		MyEntry<T, V> e = new MyEntry(x.getKey(), x.getValue());
		tree.delete((T) x.getKey());
		return e;
		
	}

	@Override
	public Entry<T, V> pollLastEntry() {
		INode x = tree.root;
		if(x == null) {
			return null;
		}
		while(x.getRightChild() != null) {
			x = x.getRightChild();
		}
		MyEntry<T, V> e = new MyEntry(x.getKey(), x.getValue());
		tree.delete((T) x.getKey());
		return e;
	}

	@Override
	public void put(T key, V value) {
		tree.insert(key, value);
	}

	@Override
	public void putAll(Map<T, V> map) {
		Set<Map.Entry<T, V>> s = map.entrySet();
		for (Entry<T, V> entry : s) {
			put(entry.getKey(), entry.getValue());
		}
		return;
	}

	@Override
	public boolean remove(T key) {
		return tree.delete(key);
	}

	@Override
	public int size() {
		return tree.size;
	}

	@Override
	public Collection<V> values() {
		Set<Map.Entry<T, V>> s =entrySet();
		List<V> c = new ArrayList<V>();
		for (Entry<T, V> entry : s) {
			c.add(entry.getValue());
		}
		return c;
	}

}
