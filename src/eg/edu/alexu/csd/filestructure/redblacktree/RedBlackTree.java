package eg.edu.alexu.csd.filestructure.redblacktree;

public class RedBlackTree<T extends Comparable<T>, V> implements IRedBlackTree<T, V> {
	
	INode<T, V> root;
	int size;
	
	public RedBlackTree() {
		size=0;
	}

	@Override
	public INode<T, V> getRoot() {
		return this.root;
	}

	@Override
	public boolean isEmpty() {
		if(size==0) {
			return true;
		}
		return false;
	}

	@Override
	public void clear() {
		root = null;
		size = 0;
	}

	@Override
	public V search(T key) {
		INode<T, V> x = root;
		while(x != null) {
			if(key.compareTo(x.getKey()) > 0) {
				x = x.getRightChild();
			}else if(key.compareTo(x.getKey()) < 0) {
				x = x.getLeftChild();
			}else {
				return x.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean contains(T key) {
		V val = search(key);
		if(val == null) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public void insert(T key, V value) {
		INode<T ,V> y = null;
		INode<T ,V> x = root;
		Node node = new Node(key ,value);
		
		while(x != null) {
			y = x;
			if(key.compareTo(x.getKey()) < 0) {
				x = x.getLeftChild();
			}else {
				x = x.getRightChild();
			}
		}
		
		node.setParent(y);
		if(y == null) {
			root = node;
		}else if(key.compareTo(y.getKey()) < 0) {
			y.setLeftChild(node);
		}else {
			y.setRightChild(node);
		}
		size++;
		insertFixUp(node);
	}

	private void insertFixUp(INode<T, V> node){
		while(node!=root && node.getParent().getColor()==true) {
			INode aunt;
			if(node.getParent() == node.getParent().getParent().getLeftChild()) { //if parent a left child
				aunt = node.getParent().getParent().getRightChild();
				
				if(aunt.getColor() == true) {  //if aunt red then {color flip}
					node.getParent().setColor(false);
					aunt.setColor(false);
					node.getParent().getParent().setColor(true);
					node = node.getParent().getParent();
				}else {       //else {rotate}
					if(node == node.getParent().getRightChild()) {  //then left - right rotation
						node = node.getParent();
						LeftRotate(node);
					}
					node.getParent().setColor(false);
					node.getParent().getParent().setColor(true);
					RightRotate(node.getParent().getParent());
				}
			}else {
				aunt = node.getParent().getParent().getLeftChild();
				
				if(aunt == null) {
					if(node == node.getParent().getLeftChild()) {  //then right - left rotation
						node = node.getParent();
						RightRotate(node);
					}
					node.getParent().setColor(false);
					node.getParent().getParent().setColor(true);
					LeftRotate(node.getParent().getParent());
				}
				else if(aunt.getColor() == true) {  //if aunt red then {color flip}
					node.getParent().setColor(false);
					aunt.setColor(false);
					node.getParent().getParent().setColor(true);
					node = node.getParent().getParent();
				}else {       //else {rotate}
					if(node == node.getParent().getLeftChild()) {  //then right - left rotation
						node = node.getParent();
						RightRotate(node);
					}
					node.getParent().setColor(false);
					node.getParent().getParent().setColor(true);
					LeftRotate(node.getParent().getParent());
				}
			}
		}
		root.setColor(false);
		return;
	}
	
	private void LeftRotate(INode<T ,V> x) {
		INode y = x.getRightChild();
		x.setRightChild(y.getLeftChild());
		
		if(y.getLeftChild() != null) {
			y.getLeftChild().setParent(x);
		}
		
		y.setParent(x.getParent());
		
		if(x.getParent() == null) {
			root = y;
		}else if(x == x.getParent().getLeftChild()) {
			x.getParent().setLeftChild(y);
		}else {
			x.getParent().setRightChild(y);
		}
		
		y.setLeftChild(x);
		x.setParent(y);
	}
	
	private void RightRotate(INode<T ,V> y) {
		INode x = y.getLeftChild();
		y.setLeftChild(x.getRightChild());
		
		if(x.getRightChild() != null) {
			x.getRightChild().setParent(y);
		}
		
		x.setParent(y.getParent());
		
		if(y.getParent() == null) {
			root = x;
		}else if(y == y.getParent().getRightChild()) {
			y.getParent().setRightChild(x);
		}else {
			y.getParent().setLeftChild(x);
		}
		
		x.setRightChild(y);
		y.setParent(x);
	}
	
	@Override
	public boolean delete(T key) {
		INode<T, V> z = searchforNode(key);
		if(z == null) {
			return false;
		}
		
		INode<T, V> y = z;
		boolean b = y.getColor();
		INode x;
		
		if(z.getLeftChild() == null) {
			x = z.getRightChild();
			Transplant(z, z.getRightChild());
		}else if(z.getRightChild() == null) {
			x = z.getLeftChild();
			Transplant(z, z.getLeftChild());
		}else {
			y = Minimum(z.getRightChild());
			b = y.getColor();
			x = y.getRightChild();
			
			if(y.getParent() == z) {
				x.setParent(y);
			}else {
				Transplant(y, y.getRightChild());
				y.setRightChild(z.getRightChild());
				y.getRightChild().setParent(y);
			}
			
			Transplant(z, y);
			y.setLeftChild(z.getLeftChild());
			y.getLeftChild().setParent(y);
			y.setColor(z.getColor());
		}
		
		if(b == false && x!=null) {
			deleteFixUP(x);
		}
		
		return true;
	}
	
	private void deleteFixUP(INode<T, V> x) {
		
		while(x!=root && x.getColor()==false) {
			if(x == x.getParent().getLeftChild()) {
				INode w = x.getParent().getRightChild();
				if(w.getColor() == true) {
					w.setColor(false);
					x.getParent().setColor(true);
					LeftRotate(x.getParent());
					w = x.getParent().getRightChild();
				}
				
				if(w.getLeftChild().getColor()==false && w.getRightChild().getColor()==false) {
					w.setColor(true);
					x = x.getParent();
				}else if(w.getRightChild().getColor()==false) {
					w.getLeftChild().setColor(false);
					w.setColor(true);
					RightRotate(w);
					w = x.getParent().getRightChild();
				}
				w.setColor(x.getParent().getColor());
				x.getParent().setColor(false);
				w.getRightChild().setColor(false);
				LeftRotate(x.getParent());
				x = root;
				
			}else {
				INode w = x.getParent().getLeftChild();
				if(w.getColor() == true) {
					w.setColor(false);
					x.getParent().setColor(true);
					RightRotate(x.getParent());
					w = x.getParent().getLeftChild();
				}
				
				if(w.getRightChild().getColor()==false && w.getLeftChild().getColor()==false) {
					w.setColor(true);
					x = x.getParent();
				}else if(w.getLeftChild().getColor()==false) {
					w.getRightChild().setColor(false);
					w.setColor(true);
					LeftRotate(w);
					w = x.getParent().getLeftChild();
				}
				w.setColor(x.getParent().getColor());
				x.getParent().setColor(false);
				w.getLeftChild().setColor(false);
				RightRotate(x.getParent());
				x = root;
			}
		}
		x.setColor(false);
	}
	
	public INode<T, V> searchforNode(T key){
		INode<T, V> x = root;
		while(x != null) {
			if(key.compareTo(x.getKey()) > 0) {
				x = x.getRightChild();
			}else if(key.compareTo(x.getKey()) < 0) {
				x = x.getLeftChild();
			}else {
				return x;
			}
		}
		return null;
	}
	
	private void Transplant(INode<T ,V> u,INode<T ,V> v) {
		if(u.getParent() == null) {
			root = v;
		}else if(u == u.getParent().getLeftChild()) {
			u.getParent().setLeftChild(v);
		}else {
			u.getParent().setRightChild(v);
		}
		
		if(v != null) {
			v.setParent(u.getParent());
		}
	}

	public INode<T, V> Minimum(INode<T, V> x){
		while(x.getLeftChild() != null) {
			x = x.getLeftChild();
		}
		return x;
	}
	
	public INode<T, V> Maximum(INode<T, V> x){
		while(x.getRightChild() != null) {
			x = x.getRightChild();
		}
		return x;
	}
	
}
